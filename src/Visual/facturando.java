package Visual;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class facturando extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField jtproducto;
    private JTextField jtcantidad;
    private JTextField jtprecio;
    private JTable table1;
    private JButton agregarButton1;
    private JComboBox buscarCliente;
    private DefaultTableModel mode1;

    public facturando() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocation(350,50);


        mode1 = new DefaultTableModel();
        mode1.addColumn("Producto");
        mode1.addColumn("Cantidad");
        mode1.addColumn("Precio");
        table1.setModel(mode1);


        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mode1.removeRow(table1.getSelectedRow());
            }

        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        agregarButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                agregarButton1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String producto  = jtproducto.getText();
                        int cantidad = Integer.parseInt(jtcantidad.getText());
                        int precio = Integer.parseInt(jtprecio.getText());
                        jtproducto.setText("");
                        jtcantidad.setText("");
                        jtprecio.setText("");
                        mode1.addRow(new Object[]{producto, cantidad, precio});

                    }
                });
            }
        });
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        facturando dialog = new facturando();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}